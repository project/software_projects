<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function software_projects_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'software_projects_all_issues_filter';
  $context->description = '';
  $context->tag = 'Software Projects';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'software_projects_issues:page_1' => 'software_projects_issues:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-deade87d2b45f7b9ef5382238c585781' => array(
          'module' => 'views',
          'delta' => 'deade87d2b45f7b9ef5382238c585781',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Software Projects');

  $export['software_projects_all_issues_filter'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'software_projects_case_block';
  $context->description = '';
  $context->tag = 'Software Projects';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'project_project' => 'project_project',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'project' => 'project',
        'project/*' => 'project/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-7ad020f7311909a9b99b31db357df455' => array(
          'module' => 'views',
          'delta' => '7ad020f7311909a9b99b31db357df455',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Software Projects');

  $export['software_projects_case_block'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'software_projects_feature_menu_tab';
  $context->description = '';
  $context->tag = 'Software Projects';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'project_issue' => 'project_issue',
        'project_project' => 'project_project',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'project' => 'project',
        'project/*' => 'project/*',
        'node/*/issues' => 'node/*/issues',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'project',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Software Projects');

  $export['software_projects_feature_menu_tab'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'software_projects_my_issues_filter';
  $context->description = '';
  $context->tag = 'Software Projects';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'software_projects_issues:page_2' => 'software_projects_issues:page_2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-dbe7501262c4cc40fe8d4e7290f8f8ee' => array(
          'module' => 'views',
          'delta' => 'dbe7501262c4cc40fe8d4e7290f8f8ee',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Software Projects');

  $export['software_projects_my_issues_filter'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'software_projects_project_issues';
  $context->description = '';
  $context->tag = 'Software Projects';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'project_project' => 'project_project',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-c8637b320a55db194a5799eca866dc39' => array(
          'module' => 'views',
          'delta' => 'c8637b320a55db194a5799eca866dc39',
          'region' => 'content',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Software Projects');

  $export['software_projects_project_issues'] = $context;
  return $export;
}
