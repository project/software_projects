<?php

/**
 * @file
 * Migration tool. Moves casetracker data to software_projects
 */

/**
 * Implementation of hook_drush_command().
 */
function software_projects_drush_command() {
  $items = array();
  $items['casetracker-migrate'] = array(
    'description' => "Converts all of your casetracker projects and cases into Software Projects projects and issues",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function software_projects_drush_help($section) {
  switch ($section) {
    case 'drush:casetracker-migrate':
      return dt("Use this command to switch from casetracker to software_projects");
      break;
  }
}

/**
 * Drush callback.
 */
function drush_software_projects_casetracker_migrate() {
  if (drush_confirm(dt("Are you sure you want to migrate all of your data from Casetracker to Software Projects? Your original data will be left in place."))) {
    drush_software_projects_casetracker_migrate_date();
  }
  else {
    drush_print('Aborted');
  }
}

/**
 * This is where the action takes place
 */
function drush_software_projects_casetracker_migrate_date() {
  $groups = db_query("SELECT node.nid AS nid FROM {node} node  LEFT JOIN {og_ancestry} og_ancestry ON node.nid = og_ancestry.nid LEFT JOIN {node} node_og_ancestry ON og_ancestry.group_nid = node_og_ancestry.nid WHERE node.type in ('group')");
  
  // Groups Loop
  while ($group = db_fetch_object($groups)) {
    drush_print($group->nid);
    $projects = db_query("SELECT node.nid AS nid FROM {node} node  LEFT JOIN {og_ancestry} og_ancestry ON node.nid = og_ancestry.nid LEFT JOIN {node} node_og_ancestry ON og_ancestry.group_nid = node_og_ancestry.nid LEFT JOIN {og_uid} node_og_ancestry__og_uid ON node_og_ancestry.nid = node_og_ancestry__og_uid.nid WHERE (node.type in ('casetracker_basic_project')) AND (node_og_ancestry__og_uid.nid = %d)", array('%d' => $group->nid));
    
    // Projects Loop
    while ($project = db_fetch_object($projects)) {
      drush_print($project->nid);
      $cases = db_query("SELECT node.nid AS nid FROM node node  LEFT JOIN casetracker_case casetracker_case ON node.vid = casetracker_case.vid LEFT JOIN node node_casetracker_case ON casetracker_case.pid = node_casetracker_case.nid WHERE (node.type in ('casetracker_basic_case')) AND (casetracker_case.pid = %d)", array("%d" => $project->nid));
      
      // Cases Loop
      while ($case = db_fetch_object($cases)) {
        drush_print($case->nid);
      }
    }
  }
}