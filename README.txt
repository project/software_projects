About
*****
Replaces Open Atrium casetracker functionality with project and project_issue.
In the future, I'll be adding version control integration to this module, so it
will become a viable alternative to Unfuddle or similar services.

Compatibility
*************
This feature was last tested with Open Atrium beta9 (released Jan 6, 2011).

Installation
************
1) Dependencies
   software_projects depends on project, project_issue, and views_customfield. The
   rest of the dependencies are included with Open Atrium. At the time of this writing,
   software_projects uses project-6.x-1.0-alpha4, project_issue-6.x-1.0-alpha4 and
   views_customfield-6.x-1.0.
   
   I will be working with the maintainers of project and project_issue to create a newer
   release so that software_projects can take advantage of things like custom priority
   levels and the plethora of other goodies that have gone into the project* modules.

2) Enable software_projects at admin/build/features

3) Enable software_projects in one or more groups and you're ready to go!

Contributing
************
I want to make software_projects awesome. Any ideas you have to that effect are welcome.
If somebody would like to build a Mylyn connector, that'd be very awesome. If somebody
would like to work with the project* maintainers to move the alpha5 release along, that
would also be awesome.

Troubleshooting
***************
Sometimes, views will throw some error after software_projects is enabled. Just clear the cache
and things will continue working.
