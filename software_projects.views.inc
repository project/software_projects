<?php

/**
 * Implementation of hook_views_handlers().
 */
function software_projects_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'software_projects') .'/includes',
    ),
    'handlers' => array(
      'software_projects_handler_field_priority' => array(
         'parent' => 'views_handler_field',
      ),
      'software_projects_handler_field_pid' => array(
         'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 * Replaces default field formatters with special-sauce field formatters.
 */
function software_projects_views_data_alter(&$data) {
	//print_r($data);
  if (isset($data['project_issues']['priority']['field']['handler'])) {
    $data['project_issues']['priority']['field']['handler'] = 'software_projects_handler_field_priority';
  }
  if (isset($data['project_issues']['pid']['field']['handler'])) {
    $data['project_issues']['pid']['field']['handler'] = 'software_projects_handler_field_pid';
  }
}
