<?php

class software_projects_handler_field_priority extends views_handler_field {
  function render($values) {
    return theme('software_projects_priority', $values->project_issues_priority);
  }
}